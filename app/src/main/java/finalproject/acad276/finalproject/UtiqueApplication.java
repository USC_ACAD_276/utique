package finalproject.acad276.finalproject;

import android.app.Application;

import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseObject;
import com.parse.ParseUser;

/**
 * Created by Chris on 11/23/15.
 */
public class UtiqueApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        //Parse Code...DO NOT DELETE
        Parse.enableLocalDatastore(this);

        //Registering our UserPick Parse subclass
        ParseObject.registerSubclass(UserPick.class);
        Parse.initialize(this, "CX3dVPSm3CBbEaGBgedpp3hu4JyjZ0S0Sb2GqaPf", "biPiMDEPf0bflCaiHUNGpC7BKEG3XxGC4uv1MeaW");

        //End Parse Code

    }

    public UtiqueApplication() {
        super();
    }
}
