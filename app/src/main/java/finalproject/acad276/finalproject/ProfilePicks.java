package finalproject.acad276.finalproject;

/**
 * Created by Chris on 12/9/15.
 *
 * This is the class that we will add info from our pool of user selects on Parse.com
 *
 */
public class ProfilePicks {
    private String name = "";
    private String brand = "";
    private String url = "";
    private String linkUrl="";

    //Constructor
    public ProfilePicks() {
        name = "";
        brand = "";
        url = "";
        linkUrl = "";
    }


    //Getters and Setters

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLinkUrl() {
        return linkUrl;
    }

    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
    }
}
