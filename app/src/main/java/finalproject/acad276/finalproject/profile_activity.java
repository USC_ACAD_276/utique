/*This program pulls in the selected clothing from Parse and generates an array of clothing with a name, brand, and url that is then sent
 * to the ProfileArrayAdapter class. */

package finalproject.acad276.finalproject;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import android.app.ListActivity;

public class profile_activity extends ListActivity {

    ListView views;
    TextView userName;


    //Declaring a new array of ProfilePicks that will be an array of these objects

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_page);

        views = (ListView) findViewById(android.R.id.list);
        userName = (TextView) findViewById(R.id.nameInput);

        ParseUser currentUser = ParseUser.getCurrentUser(); // getting current user

        userName.setText(currentUser.getUsername()); //setting userName to be the username on Parse data


        //pulls objects from Parse
        ParseQuery<ParseObject> pickPull = ParseQuery.getQuery(UserPick.CLASS_NAME);
        pickPull.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {

                //generates ProfilePicks array of picked clothing
                final ProfilePicks[] picks = new ProfilePicks[objects.size()]; //initializing the array size based on how many items are coming in from Parse

                //for loop to populate the array
                for (int i = 0; i<objects.size(); i++){ //objects.size = size of the query coming in from Parse
                    picks[i] = new ProfilePicks(); //populating each index in the array with a new ProfilePick Object

                    //creates new final array from original array that can be used in another method
                    final ProfilePicks[] picsss = picks;

                    //Setting each ProfilePick Object with its Parse.get(i) counterpart
                    picks[i].setName(objects.get(i).getString("name"));
                    picks[i].setBrand(objects.get(i).getString("brand"));
                    picks[i].setUrl(objects.get(i).getString("imageURL"));
                    picks[i].setLinkUrl(objects.get(i).getString("clickURL"));
                    views.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            String theUrl = picsss[position].getLinkUrl();
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(theUrl));
                            startActivity(browserIntent);

                        }
                    });

                }

                //sends picks array to the ProfileArrayAdapter
                ProfileArrayAdapter adapter = new ProfileArrayAdapter(getApplicationContext(), picks);
                setListAdapter(adapter);

            }
        });

    }


}

