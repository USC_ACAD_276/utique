package finalproject.acad276.finalproject;

/**
 * Created by Ben on 12/7/15.
 */

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

public class Survey extends Activity{

    Button maleButton;
    Button femaleButton;
    ImageButton submitButton;

    Button shirtXS;
    Button shirtS;
    Button shirtM;
    Button shirtL;
    Button shirtXL;

    Button pantsXS;
    Button pantsS;
    Button pantsM;
    Button pantsL;
    Button pantsXL;

    EditText shoeSize;
    EditText style;
    EditText freeTime;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.survey_page);

        maleButton = (Button) findViewById(R.id.maleButton);
        femaleButton = (Button) findViewById(R.id.femaleButton);

        shirtXS = (Button) findViewById(R.id.shirtXS);
        shirtS = (Button) findViewById(R.id.shirtS);
        shirtM = (Button) findViewById(R.id.shirtM);
        shirtL = (Button) findViewById(R.id.shirtL);
        shirtXL = (Button) findViewById(R.id.shirtXL);

        pantsXS = (Button) findViewById(R.id.pantsXS);
        pantsS = (Button) findViewById(R.id.pantsS);
        pantsM = (Button) findViewById(R.id.pantsM);
        pantsL = (Button) findViewById(R.id.pantsL);
        pantsXL = (Button) findViewById(R.id.pantsXL);

        shoeSize = (EditText) findViewById(R.id.shoeSize);
        style = (EditText) findViewById(R.id.style);
        freeTime = (EditText) findViewById(R.id.freeTime);
        submitButton = (ImageButton) findViewById(R.id.submitButton);

        //Getting currentUser
        final ParseUser currentUser = ParseUser.getCurrentUser();

        //button to submit survey information & move onto the swipe page
        submitButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // Send data to Parse.com for verification
                Intent intent = new Intent(
                        getApplicationContext(),
                        MainActivity.class);
                startActivity(intent);
            }
        });

        //toasts for female & male buttons
        maleButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "You're a male!", Toast.LENGTH_SHORT).show();
                currentUser.put("gender", "men");
                currentUser.saveInBackground();
            }
        });
        femaleButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "You're a female!", Toast.LENGTH_SHORT).show();
                currentUser.put("gender", "women");
                currentUser.saveInBackground();
            }
        });

        //toasts for shirt buttons
        shirtXS.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Extra Small", Toast.LENGTH_SHORT).show();
            }
        });
        shirtS.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Small", Toast.LENGTH_SHORT).show();
            }
        });
        shirtM.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Medium", Toast.LENGTH_SHORT).show();
            }
        });
        shirtL.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Large", Toast.LENGTH_SHORT).show();
            }
        });
        shirtXL.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Extra Large", Toast.LENGTH_SHORT).show();
            }
        });


        //toasts for pants
        pantsXS.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Extra Small", Toast.LENGTH_SHORT).show();
            }
        });
        pantsS.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Small", Toast.LENGTH_SHORT).show();
            }
        });
        pantsM.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Medium", Toast.LENGTH_SHORT).show();
            }
        });
        pantsL.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Large", Toast.LENGTH_SHORT).show();
            }
        });
        pantsXL.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Extra Large", Toast.LENGTH_SHORT).show();
            }
        });



    }


    public void saveInfo (View view){
        SharedPreferences sharedPref = getSharedPreferences("userInfo", Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();

        editor.putBoolean("maleButton", true);
        editor.putBoolean("femaleButton", true);
        editor.putBoolean("shirtXS", true);
        editor.putBoolean("shirtS", true);
        editor.putBoolean("shirtM", true);
        editor.putBoolean("shirtL", true);
        editor.putBoolean("shirtXL", true);
        editor.putBoolean("pantsXS", true);
        editor.putBoolean("pantsS", true);
        editor.putBoolean("pantsM", true);
        editor.putBoolean("pantsL", true);
        editor.putBoolean("pantsXL", true);
        editor.putString("shoeSize", shoeSize.getText().toString());

        editor.apply();

        Toast.makeText(this, "Saved!", Toast.LENGTH_LONG).show();

    }





}
