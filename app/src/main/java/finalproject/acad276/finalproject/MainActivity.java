package finalproject.acad276.finalproject;

import android.app.DownloadManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import android.content.Intent;
import com.parse.ParseAnonymousUtils;
import com.parse.ParseUser;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.parse.Parse;
import com.parse.ParseObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;

public class MainActivity extends AppCompatActivity {
//FINAL variables for searching through ShopStyle
    //SHOPSTYLE URL FORMAT
    ////    http://api.shopstyle.com/api/VERSION/METHOD_NAME?pid=YOUR_API_KEY
    //

    private final String TAG = MainActivity.class.getName();
    private final String URL = "http://api.shopstyle.com/api/v2/products?pid=uid2400-31902820-38&fts=";
    private static final String CAT = "&cat=";//Setting the gender category of our query request
    private static final String OFFSET = "&offset=";//paging through to next set of pulls
    private static final String LIMIT = "&limit=5";//limiting 5 pulls from API at a time
    private static final String LIMIT_SHORT = "&limit=1"; //When Replacing a Bottom ImageView
//    private final String KEY = "?pid=uid2400-31902820-38";

    //Storing data based on the user input and to be sent to PARSE
    private String mSearch = ""; //THIS IS WHAT THE USER SEARCHED
    private String search = ""; //This is where we plant searches to
    private String mCat = ""; //for the gender based on what the user has said in their survey
    private String requestURL = "";


    //Things we are saving when someone swipes right
    private String[] imageURL;
    private String[] name;
    private String[] brand;
    private String[] productURL;
    private String[] price;


//  Variables from XML
    ////TopSection imageBUTTONS
    private ImageButton shirtButton;
    private ImageButton swipeButton;
    private ImageButton pantsButton;
    private ImageButton dressButton;
    private ImageButton shoeButton;
    private ImageButton watchButton;

    ////Middle Section
    private ImageView mainImage;
    private TextView itemInfo;
    private ImageButton yesButton;
    private ImageButton noButton;

    ////Bot Section
    //These are the imageButtons
    private ImageView sugg1;
    private ImageView sugg2;
    private ImageView sugg3;
    private ImageView sugg4;
    private ImageView[] sugg; //array for ImageButtons


//    Volley render queue
    RequestQueue queue;


//    MISC VARIABLES
    private int counter = 0;







    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.swipe_page);

        //FOR LOG IN
        // Determine whether the current user is an anonymous user
        if (ParseUser.getCurrentUser() == null) {
            //there is no one logged in
            //there is no one here
        }

        else if (ParseAnonymousUtils.isLinked(ParseUser.getCurrentUser())) {
            // If user is anonymous, send the user to LoginSignup.class
//            Intent intent = new Intent(MainActivity.this,
//                    LoginSignup.class);
//            startActivity(intent);
//            finish();
        } else {
            // If current user is NOT anonymous user
            // Get current user data from Parse
            ParseUser currentUser = ParseUser.getCurrentUser();
            if (currentUser != null) {
                // Send logged in users to Welcome.class
//                Intent intent = new Intent(MainActivity.this, Welcome.class);
//                startActivity(intent);
//                finish();
            } else {
                // Send user to LoginSignup.class
                Intent intent = new Intent(MainActivity.this,
                        LoginSignup.class);
                startActivity(intent);
                finish();
            }
        }

        //References from XML
        swipeButton = (ImageButton) findViewById(R.id.imageButton);
        shirtButton = (ImageButton) findViewById(R.id.shirtButton);
        pantsButton = (ImageButton) findViewById(R.id.pantsButton);
        dressButton = (ImageButton) findViewById(R.id.dressButton);
        shoeButton = (ImageButton) findViewById(R.id.shoeButton);
        watchButton = (ImageButton) findViewById(R.id.watchButton);
        yesButton = (ImageButton) findViewById(R.id.yesButton);
        noButton = (ImageButton) findViewById(R.id.noButton);


        queue = Volley.newRequestQueue(this); //Volley told me i have to do this

        //Getting Current User
        ParseUser currentUser = ParseUser.getCurrentUser();
        mCat = CAT + currentUser.getString("gender"); //Setting currentUser Gender and putting a category constraint on our Shopstyle Queries


        //Loading in the first set of images
        search = "shirt";
        requestURL = URL + search + mCat + OFFSET + counter + LIMIT; //Building the URL
        shopstyle(requestURL); //running the function
        counter = counter + 5; //so i can offset to the next 5 results
        Log.d("URL", requestURL); //Logging what the URL is.


        swipeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(
                        getApplicationContext(),
                        profile_activity.class); // to profile Screen
                startActivity(intent);
            }
        });


        //ImageButton OnClickListeners --> TOP OF SCREEN BUTTON BAR
            shirtButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    search = "shirt";
                    requestURL = URL + search + mCat + OFFSET + counter + LIMIT; //Building the URL
                    shopstyle(requestURL); //running the function
                    counter = counter + 5; //so i can offset to the next 5 results
                    Log.d("URL", requestURL);
                }
            });
            pantsButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    search = "pants";
                    requestURL = URL + search + mCat + OFFSET + counter + LIMIT;
                    shopstyle(requestURL);
                    counter = counter + 5;
                    Log.d("URL", requestURL);
                }
            });
            dressButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    search = "dress";
                    requestURL = URL + search + mCat + OFFSET + counter + LIMIT;
                    shopstyle(requestURL);
                    counter = counter + 5;
                    Log.d("URL", requestURL);
                }
            });
            shoeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    search = "shoes";
                    requestURL = URL + search + mCat + OFFSET + counter + LIMIT;
                    shopstyle(requestURL);
                    counter = counter + 5;
                    Log.d("URL", requestURL);
                }
            });
            watchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    search = "watch";
                    requestURL = URL + search + mCat + OFFSET + counter + LIMIT;
                    shopstyle(requestURL);
                    counter = counter + 5;
                    Log.d("URL", requestURL);
                }
            });

        // MORE IMAGE BUTTON onClickListners for choice buttons on the side
            yesButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //OUTPUT TO PARSE
                    UserPick toParse = new UserPick();
                    //saving only primary image which will always be array index 0 for our app
                    toParse.setpName(name[0]);
                    toParse.setpImageURL(imageURL[0]);
                    toParse.setpBrand(brand[0]);
                    toParse.setpProductURL(productURL[0]);
                    toParse.setpPrice(price[0]);
                    try {
                        toParse.saveData();
                    } catch (com.parse.ParseException e) {
                        e.printStackTrace();
                    }
                    requestURL = URL + search + mCat + OFFSET + counter + LIMIT;
                    shopstyle(requestURL);
                    counter = counter + 5;
                }
            });
            noButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    requestURL = URL + search + mCat + OFFSET + counter + LIMIT;
                    shopstyle(requestURL);
                    counter = counter + 5;
                }
            });


    }

    public void shopstyle(final String reqURL){
        mainImage = (ImageView) findViewById(R.id.sugg0);
        itemInfo = (TextView) findViewById(R.id.itemInfo);
        sugg1 = (ImageView) findViewById(R.id.sugg1);
        sugg2 = (ImageView) findViewById(R.id.sugg2);
        sugg3 = (ImageView) findViewById(R.id.sugg3);
        sugg4 = (ImageView) findViewById(R.id.sugg4);
        sugg = new ImageView[4];


        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, reqURL, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                    //declaring arrays to be saved to
                        int loopLimit = 5; //array size control
                        name = new String[loopLimit];
                        imageURL = new String[loopLimit];
                        brand = new String[loopLimit];
                        productURL = new String[loopLimit];
                        price = new String[loopLimit];

                        try {
                        //Parsing the ShopStyle
                        ////Need to for loop so I can go through each set from ShopStyle
                            for (int i=0; i<loopLimit; i++) {
                                JSONArray products = response.getJSONArray("products");
                                JSONObject dress1 = products.getJSONObject(i);
                                name[i] = dress1.get("name").toString(); //comes from the main class
                                JSONObject image = dress1.getJSONObject("image");
                                JSONObject size = image.getJSONObject("sizes");
                                JSONObject iphone = size.getJSONObject("Medium"); //selecting which image size
                                imageURL[i] = iphone.get("url").toString(); //Images come in URL form

                                //Additional info to be sent to Parse
                                //else statements are to fill in the parameter
                                    if (dress1.has("brand")){ //Getting brand
                                        JSONObject company = dress1.getJSONObject("brand");
                                        brand[i] = company.getString("name"); //Brand name
                                    }
                                    else {
                                        brand[i] = "none";
                                    }
                                    if (dress1.has("clickUrl")){ //Getting Click Urls
                                        productURL[i] = dress1.getString("clickUrl");
                                    }
                                    else{
                                        brand[i] = "none";
                                    }
                                    if (dress1.has("priceLabel")) { //Getting Price
                                        price[i] = dress1.getString("priceLabel");
                                    }
                                    else{
                                        price[i] = "none";
                                    }


                                //Output to stuff
                                ////Like TextView or ImageViews
                                itemInfo.setText(name[0]); //main section

                            } //end of for-loop

                                //Ghetto version of putting images into the bottom suggestion bar
                                    Glide.with(MainActivity.this)
                                            .load(imageURL[1])
                                            .into(sugg1);
                                    Glide.with(MainActivity.this)
                                            .load(imageURL[2])
                                            .into(sugg2);
                                    Glide.with(MainActivity.this)
                                            .load(imageURL[3])
                                            .into(sugg3);
                                    Glide.with(MainActivity.this)
                                            .load(imageURL[4])
                                            .into(sugg4);

                            //Gliding into main image
                            Glide.with(MainActivity.this)
                                    .load(imageURL[0])
                                    .listener(new RequestListener<String, GlideDrawable>() {
                                        @Override
                                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                            e.printStackTrace();
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                            return false;
                                        }
                                    })
                                    .error(R.drawable.dress)
                                    .placeholder(R.drawable.pants)
                                    .into(mainImage);


                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
                        }

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        queue.add(jsObjRequest);
    }

}


