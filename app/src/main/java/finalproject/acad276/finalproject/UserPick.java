package finalproject.acad276.finalproject;

import com.parse.ParseACL;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;

/**
 * Created by Chris on 12/2/15.
 * THIS IS THE CLASS THAT SAVES TO PARSE
 */
@ParseClassName(UserPick.CLASS_NAME)
public class UserPick extends ParseObject {
    public static final String CLASS_NAME = "UserPick";

    //Things we want to save on user pick
    private String pImageURL="";
    private String pName = "";
    private String pBrand ="";
    private String pProductURL = "";
    private String pPrice = "";


//GETTERS AND SETTERS
    public String getpImageURL() {
        return pImageURL;
    }

    public void setpImageURL(String pImageURL) {
        this.pImageURL = pImageURL;
    }

    public String getpName() {
        return pName;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }

    public String getpBrand() {
        return pBrand;
    }

    public void setpBrand(String pBrand) {
        this.pBrand = pBrand;
    }

    public String getpProductURL() {
        return pProductURL;
    }

    public void setpProductURL(String pProductURL) {
        this.pProductURL = pProductURL;
    }

    public String getpPrice() {
        return pPrice;
    }

    public void setpPrice(String pPrice) {
        this.pPrice = pPrice;
    }


    public void saveData() throws ParseException {
        put("name", pName);
        put("imageURL", pImageURL);
        put("brand", pBrand);
        put("clickURL", pProductURL);
        put("price", pPrice);

        setACL(new ParseACL(ParseUser.getCurrentUser()));
        saveInBackground();
    }




}
