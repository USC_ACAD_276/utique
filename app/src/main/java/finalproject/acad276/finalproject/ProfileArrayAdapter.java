/*This is a custom adapter that pulls in the data from the picks array and populates the imageviews and textviews of our
 custom layout with the appropriate data */

package finalproject.acad276.finalproject;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

/**
 * Created by Chris on 12/9/15.
 */
public class ProfileArrayAdapter extends ArrayAdapter<ProfilePicks>{
    private final Context context;
    private final ProfilePicks[] picks;

//imports context and the picks array
    public ProfileArrayAdapter(Context context, ProfilePicks[] picks){
        super(context, R.layout.profile_page, picks);
        this.context = context;
        this.picks = picks;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //generates a rowview according to the listview_layout and populates it with the strings from the array
        View rowView = inflater.inflate(R.layout.listview_layout, parent, false);
        TextView textView = (TextView) rowView.findViewById(R.id.name);
        TextView textView1 = (TextView) rowView.findViewById(R.id.brand);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.smallPic);
        Glide.with(context).
                load(picks[position].getUrl()).
                into(imageView);
        textView.setText(picks[position].getName());
        textView1.setText(picks[position].getBrand());


//        rowView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//             final Intent browserIntent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("http://www.google.com"));
//                context.startActivity(browserIntent);
//            }
//        });


        return rowView;
    }


}
