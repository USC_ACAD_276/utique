package finalproject.acad276.finalproject;

/**
 * Created by Ben on 12/7/15.
 */
import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseUser;
import android.app.Application;

public class ParseApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        // Add your initialization code here
        Parse.initialize(this, "Mmw6g4fXIuB3e98gDlCqbl8x4ndI72B3CeUJbWcW", "3W8gJOkjCf2Uj6zvX2FzNyD5NAQ3wtDUHmzweqGy");

        ParseUser.enableAutomaticUser();
        ParseACL defaultACL = new ParseACL();

        // If you would like all objects to be private by default, remove this
        // line.
        defaultACL.setPublicReadAccess(true);

        ParseACL.setDefaultACL(defaultACL, true);
    }

}

