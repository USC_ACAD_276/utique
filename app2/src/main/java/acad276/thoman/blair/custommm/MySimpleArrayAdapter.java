package acad276.thoman.blair.custommm;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class MySimpleArrayAdapter extends ArrayAdapter<ClothingData> {
    private final Context context;
    private final ClothingData[] values;

    public MySimpleArrayAdapter(Context context, ClothingData[] values) {
        super(context, R.layout.activity_main, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.activity_main, parent, false);
        TextView textView = (TextView) rowView.findViewById(R.id.label);
        TextView textView1 = (TextView) rowView.findViewById(R.id.secondLabel);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
        Glide.with(context).
                load(values[position].getUrl()).
                into(imageView);
        textView.setText(values[position].getLabel());
        textView1.setText(values[position].getSecondLabel());


        return rowView;
    }
}