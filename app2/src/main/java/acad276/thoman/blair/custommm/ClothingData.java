package acad276.thoman.blair.custommm;

/**
 * Created by blairthoman on 12/9/15.
 */
public class ClothingData {
    private String label;
    private String url;
    private String secondLabel;

    //constructor

    public ClothingData(){
        label = "";
        url = "";
        secondLabel = "";
    }

    //getters and setters

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSecondLabel() {
        return secondLabel;
    }

    public void setSecondLabel(String secondLabel) {
        this.secondLabel = secondLabel;
    }
}
