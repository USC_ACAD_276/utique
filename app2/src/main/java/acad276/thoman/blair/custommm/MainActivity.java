package acad276.thoman.blair.custommm;

import android.app.ListActivity;
import android.os.Bundle;

public class MainActivity extends ListActivity {
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        String[] values = new String[] {"dog", "cat", "fish" };

        String[] values2 = new String[] {"big", "small", "little"};

        String[] urls = new String[] {"https://pbs.twimg.com/profile_images/616542814319415296/McCTpH_E.jpg",
                "http://static5.businessinsider.com/image/511d104a69bedd1f7c000012/grumpy-cat-definitely-did-not-make-100-million.jpg",
                "https://pbs.twimg.com/profile_images/616542814319415296/McCTpH_E.jpg"};

        ClothingData[] myClothes = new ClothingData[values.length];
        for (int i = 0; i < values.length; i++){
            myClothes[i] = new ClothingData();
        }

        for (int i = 0; i < values.length; i++){
            String theLabel = values[i];
            String theSecondLabel = values2[i];
            String theUrl = urls[i];
            myClothes[i].setLabel(theLabel);
            myClothes[i].setSecondLabel(theSecondLabel);
            myClothes[i].setUrl(theUrl);
        }


        MySimpleArrayAdapter adapter = new MySimpleArrayAdapter(getApplicationContext(), myClothes);
        setListAdapter(adapter);



    }

}